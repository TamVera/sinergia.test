﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(sinergia.tamara.Startup))]
namespace sinergia.tamara
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
