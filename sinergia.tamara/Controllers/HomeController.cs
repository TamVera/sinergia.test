﻿using datos.inventario;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace sinergia.tamara.Controllers
{
    public class HomeController : Controller
    {
        MoldeSinergiaInventario _context = new MoldeSinergiaInventario();
        //http://localhost:5086/api/producto/
        string Baseurl = "http://localhost:5086/";

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        //public ActionResult producto()
        //{
        //    var listofData = _context.ING_PRODUCTO.ToList();
        //    return View(listofData);
        //}


        public async Task<ActionResult> producto()
        {
            List<ING_PRODUCTO> dataProducto = new List<ING_PRODUCTO>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //llama a los productos
                HttpResponseMessage res = await client.GetAsync("api/producto/");
                if (res.IsSuccessStatusCode)
                {
                    var proResponse = res.Content.ReadAsStringAsync().Result;
                    dataProducto = JsonConvert.DeserializeObject<List<ING_PRODUCTO>>(proResponse);
                }
                return View(dataProducto);
            }

        }

        public async Task<ActionResult> crearproducto()
        {

            #region ..Categoria

            List<ING_CATEGORIA> dataCategoria = new List<ING_CATEGORIA>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //llama a los productos
                HttpResponseMessage res = await client.GetAsync("api/categoria/");
                if (res.IsSuccessStatusCode)
                {
                    var proResponse = res.Content.ReadAsStringAsync().Result;
                    dataCategoria = JsonConvert.DeserializeObject<List<ING_CATEGORIA>>(proResponse);
                }

            }

            List<SelectListItem> lSelectcategoria = new List<SelectListItem>();
            foreach (ING_CATEGORIA item in dataCategoria)
            {
                lSelectcategoria.Add(new SelectListItem() { Text = item.ING_DESCRIPCION_CATEGORIA, Value = item.ING_CATEGORIA_ID.ToString() });
            }

            lSelectcategoria.First().Selected = true;

            ViewBag.categoria = new SelectList(lSelectcategoria, "Value", "Text");

            #endregion

            #region ..Medida

            List<ING_MEDIDA> dataMedida = new List<ING_MEDIDA>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //llama a los productos
                HttpResponseMessage res = await client.GetAsync("api/medida/");
                if (res.IsSuccessStatusCode)
                {
                    var proResponse = res.Content.ReadAsStringAsync().Result;
                    dataMedida = JsonConvert.DeserializeObject<List<ING_MEDIDA>>(proResponse);
                }

            }

            List<SelectListItem> lSelectMedida = new List<SelectListItem>();
            foreach (ING_MEDIDA item in dataMedida)
            {
                lSelectMedida.Add(new SelectListItem() { Text = item.ING_DETALLE_MEDIDA, Value = item.ING_ID_MEDIDA.ToString() });
            }

            lSelectMedida.First().Selected = true;

            ViewBag.medida = new SelectList(lSelectMedida, "Value", "Text");

            #endregion

            #region ..Proveedor

            List<ING_PROVEEDOR> dataProveedora = new List<ING_PROVEEDOR>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //llama a los productos
                HttpResponseMessage res = await client.GetAsync("api/proveedor/");
                if (res.IsSuccessStatusCode)
                {
                    var proResponse = res.Content.ReadAsStringAsync().Result;
                    dataProveedora = JsonConvert.DeserializeObject<List<ING_PROVEEDOR>>(proResponse);
                }

            }

            List<SelectListItem> lSelectProveedor = new List<SelectListItem>();
            foreach (ING_PROVEEDOR item in dataProveedora)
            {
                lSelectProveedor.Add(new SelectListItem() { Text = item.ING_DETALLE_PROVEEDOR, Value = item.ING_PROVEEDOR_ID.ToString() });
            }

            lSelectProveedor.First().Selected = true;

            ViewBag.proveedor = new SelectList(lSelectProveedor, "Value", "Text");

            #endregion


            #region ..Marca

            List<ING_MARCA> dataMarcara = new List<ING_MARCA>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //llama a los productos
                HttpResponseMessage res = await client.GetAsync("api/marca/");
                if (res.IsSuccessStatusCode)
                {
                    var proResponse = res.Content.ReadAsStringAsync().Result;
                    dataMarcara = JsonConvert.DeserializeObject<List<ING_MARCA>>(proResponse);
                }

            }

            List<SelectListItem> lSelectMarca = new List<SelectListItem>();
            foreach (ING_MARCA item in dataMarcara)
            {
                lSelectMarca.Add(new SelectListItem() { Text = item.ING_DETALLE_MARCA, Value = item.ING_MARCA_ID.ToString() });
            }

            lSelectMarca.First().Selected = true;

            ViewBag.marca = new SelectList(lSelectMarca, "Value", "Text");

            #endregion

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> crearproducto(ING_PRODUCTO model)
        {
            //_context.ING_PRODUCTO.Add(model);
            //_context.SaveChanges();
            //ViewBag.Message = "Data Insert Successfully";

            //List<ING_PRODUCTO> dataProducto = new List<ING_PRODUCTO>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl+ "api/producto/");
                var postTask = client.PostAsJsonAsync<ING_PRODUCTO>("productos", model);
                //client.DefaultRequestHeaders.Clear();
                //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                postTask.Wait();
                var res = postTask.Result;
                //HttpResponseMessage res = await client.GetAsync("api/producto/");
                if (res.IsSuccessStatusCode)
                {
                    return RedirectToAction("producto");
                    //var proResponse = res.Content.ReadAsStringAsync().Result;
                    //dataProducto = JsonConvert.DeserializeObject<List<ING_PRODUCTO>>(proResponse);
                }
                //return View(dataProducto);
            }
            ModelState.AddModelError(string.Empty, "Error, contacto con el jefe de area");
            return View(model);
        }

        public async Task<ActionResult> EditarProducto(int id)
        {
            #region ..Categoria

            List<ING_CATEGORIA> dataCategoria = new List<ING_CATEGORIA>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //llama a los productos
                HttpResponseMessage res = await client.GetAsync("api/categoria/");
                if (res.IsSuccessStatusCode)
                {
                    var proResponse = res.Content.ReadAsStringAsync().Result;
                    dataCategoria = JsonConvert.DeserializeObject<List<ING_CATEGORIA>>(proResponse);
                }

            }

            List<SelectListItem> lSelectcategoria = new List<SelectListItem>();
            foreach (ING_CATEGORIA item in dataCategoria)
            {
                lSelectcategoria.Add(new SelectListItem() { Text = item.ING_DESCRIPCION_CATEGORIA, Value = item.ING_CATEGORIA_ID.ToString() });
            }

            lSelectcategoria.First().Selected = true;

            ViewBag.categoria = new SelectList(lSelectcategoria, "Value", "Text");

            #endregion

            #region ..Medida

            List<ING_MEDIDA> dataMedida = new List<ING_MEDIDA>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //llama a los productos
                HttpResponseMessage res = await client.GetAsync("api/medida/");
                if (res.IsSuccessStatusCode)
                {
                    var proResponse = res.Content.ReadAsStringAsync().Result;
                    dataMedida = JsonConvert.DeserializeObject<List<ING_MEDIDA>>(proResponse);
                }

            }

            List<SelectListItem> lSelectMedida = new List<SelectListItem>();
            foreach (ING_MEDIDA item in dataMedida)
            {
                lSelectMedida.Add(new SelectListItem() { Text = item.ING_DETALLE_MEDIDA, Value = item.ING_ID_MEDIDA.ToString() });
            }

            lSelectMedida.First().Selected = true;

            ViewBag.medida = new SelectList(lSelectMedida, "Value", "Text");

            #endregion

            #region ..Proveedor

            List<ING_PROVEEDOR> dataProveedora = new List<ING_PROVEEDOR>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //llama a los productos
                HttpResponseMessage res = await client.GetAsync("api/proveedor/");
                if (res.IsSuccessStatusCode)
                {
                    var proResponse = res.Content.ReadAsStringAsync().Result;
                    dataProveedora = JsonConvert.DeserializeObject<List<ING_PROVEEDOR>>(proResponse);
                }

            }

            List<SelectListItem> lSelectProveedor = new List<SelectListItem>();
            foreach (ING_PROVEEDOR item in dataProveedora)
            {
                lSelectProveedor.Add(new SelectListItem() { Text = item.ING_DETALLE_PROVEEDOR, Value = item.ING_PROVEEDOR_ID.ToString() });
            }

            lSelectProveedor.First().Selected = true;

            ViewBag.proveedor = new SelectList(lSelectProveedor, "Value", "Text");

            #endregion


            #region ..Marca

            List<ING_MARCA> dataMarcara = new List<ING_MARCA>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //llama a los productos
                HttpResponseMessage res = await client.GetAsync("api/marca/");
                if (res.IsSuccessStatusCode)
                {
                    var proResponse = res.Content.ReadAsStringAsync().Result;
                    dataMarcara = JsonConvert.DeserializeObject<List<ING_MARCA>>(proResponse);
                }

            }

            List<SelectListItem> lSelectMarca = new List<SelectListItem>();
            foreach (ING_MARCA item in dataMarcara)
            {
                lSelectMarca.Add(new SelectListItem() { Text = item.ING_DETALLE_MARCA, Value = item.ING_MARCA_ID.ToString() });
            }

            lSelectMarca.First().Selected = true;

            ViewBag.marca = new SelectList(lSelectMarca, "Value", "Text");

            #endregion

            ING_PRODUCTO producto = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                var responseTask = client.GetAsync($"api/producto/{id}");
                //client.DefaultRequestHeaders.Clear();
                //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                responseTask.Wait();
                var res = responseTask.Result;
                if (res.IsSuccessStatusCode)
                {
                    var readTask = res.Content.ReadAsAsync<ING_PRODUCTO>();
                    readTask.Wait();
                    producto = readTask.Result;

                    //dataProducto = JsonConvert.DeserializeObject<List<ING_PRODUCTO>>(proResponse);
                }
            }
            return View(producto);

        }

        [HttpPost]
        public ActionResult EditarProducto(ING_PRODUCTO model)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                var putTask = client.PutAsJsonAsync<ING_PRODUCTO>($"api/producto/{model.ING_PRODUCTO_CODIGO}", model);
                //client.DefaultRequestHeaders.Clear();
                //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                putTask.Wait();
                var res = putTask.Result;
                if (res.IsSuccessStatusCode)
                {
                    return RedirectToAction("producto");
                    //var proResponse = res.Content.ReadAsStringAsync().Result;
                    //dataProducto = JsonConvert.DeserializeObject<List<ING_PRODUCTO>>(proResponse);
                }
            }
            return View(model);
        }
    }
}