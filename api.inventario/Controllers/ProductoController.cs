﻿using datos.inventario;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace api.inventario.Controllers
{
    public class ProductoController : ApiController
    {
        private MoldeSinergiaInventario _context = new MoldeSinergiaInventario();

        //Visualiza todos los registros (api/producto/)  
        [HttpGet]
        public IEnumerable<ING_PRODUCTO> Get()
        {
            List<ING_PRODUCTO> lstDatos;
            ING_MARCA marca = new ING_MARCA();
            ING_CATEGORIA categoria = new ING_CATEGORIA();
            ING_MEDIDA medida = new ING_MEDIDA();
            ING_PROVEEDOR proveedor = new ING_PROVEEDOR();

            //SqlParameter param = new SqlParameter("@origen", origen);
            //SqlParameter param1 = new SqlParameter("@periodo", periodo);
            SqlParameter paramMarca = null;
            SqlParameter paramCategoria = null;
            SqlParameter paramMedida = null;
            SqlParameter paramProveedor = null;

            lstDatos = _context.Database.SqlQuery<ING_PRODUCTO>("dbo.SP_PRODUCTO /*@origen, @periodo*/"
                                                                    /*, param, param1*/).ToList();
            foreach (ING_PRODUCTO item in lstDatos)
            {
                paramMarca = new SqlParameter("@P_ID_MARCA", item.ING_MARCA_ID);
                marca = _context.Database.SqlQuery<ING_MARCA>("dbo.SP_MARCA @P_ID_MARCA /*, @periodo*/"
                                                                    , paramMarca/*, param1*/).ToList().FirstOrDefault();

                paramCategoria = new SqlParameter("@P_ID_CATEGORIA", item.ING_CATEGORIA_ID);
                categoria = _context.Database.SqlQuery<ING_CATEGORIA>("dbo.SP_CATEGORIA @P_ID_CATEGORIA /*, @periodo*/"
                                                                    , paramCategoria/*, param1*/).ToList().FirstOrDefault();

                paramMedida = new SqlParameter("@P_ID_MEDIDA", item.ING_MEDIDA_ID);
                medida = _context.Database.SqlQuery<ING_MEDIDA>("dbo.SP_MEDIDA @P_ID_MEDIDA /*, @periodo*/"
                                                                    , paramMedida/*, param1*/).ToList().FirstOrDefault();

                paramProveedor = new SqlParameter("@P_ID_PROVEEDOR", item.ING_PROVEEDOR_ID);
                proveedor = _context.Database.SqlQuery<ING_PROVEEDOR>("dbo.SP_PROVEEDOR @P_ID_PROVEEDOR /*, @periodo*/"
                                                                    , paramProveedor/*, param1*/).ToList().FirstOrDefault();


                lstDatos.Find(x => x.ING_PRODUCTO_ID == item.ING_PRODUCTO_ID).ING_MARCA = marca;

                lstDatos.Find(x => x.ING_PRODUCTO_ID == item.ING_PRODUCTO_ID).ING_CATEGORIA = categoria;
                lstDatos.Find(x => x.ING_PRODUCTO_ID == item.ING_PRODUCTO_ID).ING_MEDIDA = medida;
                lstDatos.Find(x => x.ING_PRODUCTO_ID == item.ING_PRODUCTO_ID).ING_PROVEEDOR =  proveedor;

            }



            //return _context.ING_PRODUCTO.ToList();
            return lstDatos;

        }

        [HttpGet]
        public ING_PRODUCTO Get(int id)
        {
            List<ING_PRODUCTO> lstDatos;
            ING_PRODUCTO producto;

            ING_MARCA marca = new ING_MARCA();
            ING_CATEGORIA categoria = new ING_CATEGORIA();
            ING_MEDIDA medida = new ING_MEDIDA();
            ING_PROVEEDOR proveedor = new ING_PROVEEDOR();

            //SqlParameter param = new SqlParameter("@origen", origen);
            //SqlParameter param1 = new SqlParameter("@periodo", periodo);
            SqlParameter paramProducto = null;

            SqlParameter paramMarca = null;
            SqlParameter paramCategoria = null;
            SqlParameter paramMedida = null;
            SqlParameter paramProveedor = null;

            paramProducto = new SqlParameter("@P_ID_PRODUCTO", id);
            lstDatos = _context.Database.SqlQuery<ING_PRODUCTO>("dbo.SP_PRODUCTO @P_ID_PRODUCTO/*, @periodo*/"
                                                                    , paramProducto/*, param1*/).ToList();
            foreach (ING_PRODUCTO item in lstDatos)
            {
                paramMarca = new SqlParameter("@P_ID_MARCA", item.ING_MARCA_ID);
                marca = _context.Database.SqlQuery<ING_MARCA>("dbo.SP_MARCA @P_ID_MARCA /*, @periodo*/"
                                                                    , paramMarca/*, param1*/).ToList().FirstOrDefault();

                paramCategoria = new SqlParameter("@P_ID_CATEGORIA", item.ING_CATEGORIA_ID);
                categoria = _context.Database.SqlQuery<ING_CATEGORIA>("dbo.SP_CATEGORIA @P_ID_CATEGORIA /*, @periodo*/"
                                                                    , paramCategoria/*, param1*/).ToList().FirstOrDefault();

                paramMedida = new SqlParameter("@P_ID_MEDIDA", item.ING_MEDIDA_ID);
                medida = _context.Database.SqlQuery<ING_MEDIDA>("dbo.SP_MEDIDA @P_ID_MEDIDA /*, @periodo*/"
                                                                    , paramMedida/*, param1*/).ToList().FirstOrDefault();

                paramProveedor = new SqlParameter("@P_ID_PROVEEDOR", item.ING_PROVEEDOR_ID);
                proveedor = _context.Database.SqlQuery<ING_PROVEEDOR>("dbo.SP_PROVEEDOR @P_ID_PROVEEDOR /*, @periodo*/"
                                                                    , paramProveedor/*, param1*/).ToList().FirstOrDefault();


                lstDatos.Find(x => x.ING_PRODUCTO_ID == item.ING_PRODUCTO_ID).ING_MARCA = marca;

                lstDatos.Find(x => x.ING_PRODUCTO_ID == item.ING_PRODUCTO_ID).ING_CATEGORIA = categoria;
                lstDatos.Find(x => x.ING_PRODUCTO_ID == item.ING_PRODUCTO_ID).ING_MEDIDA = medida;
                lstDatos.Find(x => x.ING_PRODUCTO_ID == item.ING_PRODUCTO_ID).ING_PROVEEDOR = proveedor;

            }

            producto = lstDatos.FirstOrDefault();

            //return _context.ING_PRODUCTO.ToList();
            return producto;

        }

        [HttpPost]
        public IHttpActionResult agregarproducto([FromBody]ING_PRODUCTO pro )
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ING_PRODUCTO prodductonew = new ING_PRODUCTO();
                    prodductonew.ING_CANTIDAD = pro.ING_CANTIDAD;
                    prodductonew.ING_CATEGORIA_ID = pro.ING_CATEGORIA_ID;
                    prodductonew.ING_MARCA_ID = pro.ING_MARCA_ID;
                    prodductonew.ING_MEDIDA_ID = pro.ING_MEDIDA_ID;
                    prodductonew.ING_PROVEEDOR_ID = pro.ING_PROVEEDOR_ID;
                    prodductonew.ING_DESCRIPCION = pro.ING_DESCRIPCION;
                    prodductonew.ING_PRODUCTO_CODIGO = pro.ING_PRODUCTO_CODIGO;
                    prodductonew.ING_PRECIO_UNITARIO = pro.ING_PRECIO_UNITARIO;

                    _context.ING_PRODUCTO.Add(prodductonew);
                    _context.SaveChanges();
                    return Ok(pro);
                }
                catch (Exception ex)
                {
                    //ex.InnerException
                    return BadRequest();
                }

                
                
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPut]
        public IHttpActionResult actualizarproducto(string id, [FromBody]ING_PRODUCTO pro)
        {
            if (ModelState.IsValid)
            {
                ING_PRODUCTO productoExiste = null;

                productoExiste = (from producto in _context.ING_PRODUCTO
                                      where producto.ING_PRODUCTO_CODIGO == id
                                      select producto).ToList().FirstOrDefault();
                if (productoExiste != null)
                {

                    try
                    {
                        _context.Entry(productoExiste).State = EntityState.Modified;

                        productoExiste.ING_CANTIDAD = pro.ING_CANTIDAD;
                        productoExiste.ING_PRECIO_UNITARIO = pro.ING_PRECIO_UNITARIO;
                        //productoExiste.ING_PRODUCTO_CODIGO = pro.ING_PRODUCTO_CODIGO;
                        productoExiste.ING_DESCRIPCION = pro.ING_DESCRIPCION;
                        productoExiste.ING_CATEGORIA_ID = pro.ING_CATEGORIA_ID;
                        productoExiste.ING_MARCA_ID = pro.ING_MARCA_ID;
                        productoExiste.ING_MEDIDA_ID = pro.ING_MEDIDA_ID;
                        productoExiste.ING_PROVEEDOR_ID = pro.ING_PROVEEDOR_ID;


                        //_context.Entry(pro).State = EntityState.Modified;
                        _context.SaveChanges();
                        return Ok();
                        
                    }
                    catch (Exception ex)
                    {
                        //ex.InnerException
                        return BadRequest();
                    }

                }
                else
                {
                    return NotFound();
                }

            }
            else
            {
                return BadRequest();
            }
            
        }
    }
}
