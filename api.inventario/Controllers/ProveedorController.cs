﻿using datos.inventario;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace api.inventario.Controllers
{
    public class ProveedorController : ApiController
    {
        private MoldeSinergiaInventario _context = new MoldeSinergiaInventario();

        [HttpGet]
        public IEnumerable<ING_PROVEEDOR> Get()
        {
            //ING_MARCA marca = new ING_MARCA();
            //List<ING_CATEGORIA> categoria = new List<ING_CATEGORIA>();
            List<ING_PROVEEDOR> proveedor = new List<ING_PROVEEDOR>();
            //ING_PROVEEDOR proveedor = new ING_PROVEEDOR();

            //SqlParameter paramMarca = null;
            //SqlParameter paramCategoria = null;
            //SqlParameter paramMedida = null;
            SqlParameter paramProveedor = null;

            paramProveedor = new SqlParameter("@P_ID_PROVEEDOR", DBNull.Value);
            proveedor = _context.Database.SqlQuery<ING_PROVEEDOR>("dbo.SP_PROVEEDOR /*@origen, @periodo*/"
                                                                    , paramProveedor/*, param1*/).ToList();

            return proveedor;

        }
    }
}
