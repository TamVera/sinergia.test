﻿using datos.inventario;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace api.inventario.Controllers
{
    public class CategoriaController : ApiController
    {

        private MoldeSinergiaInventario _context = new MoldeSinergiaInventario();

        [HttpGet]
        public IEnumerable<ING_CATEGORIA> Get()
        {
            //List<ING_PRODUCTO> lstDatos;
            //ING_MARCA marca = new ING_MARCA();
            List<ING_CATEGORIA> categoria = new List<ING_CATEGORIA>();
            //ING_MEDIDA medida = new ING_MEDIDA();
            //ING_PROVEEDOR proveedor = new ING_PROVEEDOR();

            //SqlParameter paramMarca = null;
            SqlParameter paramCategoria = null;
            //SqlParameter paramMedida = null;
            //SqlParameter paramProveedor = null;

            paramCategoria = new SqlParameter("@P_ID_CATEGORIA", DBNull.Value);
            categoria = _context.Database.SqlQuery<ING_CATEGORIA>("dbo.SP_CATEGORIA /*@origen, @periodo*/"
                                                                    , paramCategoria/*, param1*/).ToList();
            //foreach (ING_PRODUCTO item in lstDatos)
            //{
            //    paramMarca = new SqlParameter("@P_ID_MARCA", item.ING_MARCA_ID);
            //    marca = _context.Database.SqlQuery<ING_MARCA>("dbo.SP_MARCA @P_ID_MARCA /*, @periodo*/"
            //                                                        , paramMarca/*, param1*/).ToList().FirstOrDefault();

            //    paramCategoria = new SqlParameter("@P_ID_CATEGORIA", item.ING_CATEGORIA_ID);
            //    categoria = _context.Database.SqlQuery<ING_CATEGORIA>("dbo.SP_CATEGORIA @P_ID_CATEGORIA /*, @periodo*/"
            //                                                        , paramCategoria/*, param1*/).ToList().FirstOrDefault();

            //    paramMedida = new SqlParameter("@P_ID_MEDIDA", item.ING_MEDIDA_ID);
            //    medida = _context.Database.SqlQuery<ING_MEDIDA>("dbo.SP_MEDIDA @P_ID_MEDIDA /*, @periodo*/"
            //                                                        , paramMedida/*, param1*/).ToList().FirstOrDefault();

            //    paramProveedor = new SqlParameter("@P_ID_PROVEEDOR", item.ING_PROVEEDOR_ID);
            //    proveedor = _context.Database.SqlQuery<ING_PROVEEDOR>("dbo.SP_PROVEEDOR @P_ID_PROVEEDOR /*, @periodo*/"
            //                                                        , paramProveedor/*, param1*/).ToList().FirstOrDefault();


            //    lstDatos.Find(x => x.ING_PRODUCTO_ID == item.ING_PRODUCTO_ID).ING_MARCA = marca;

            //    lstDatos.Find(x => x.ING_PRODUCTO_ID == item.ING_PRODUCTO_ID).ING_CATEGORIA = categoria;
            //    lstDatos.Find(x => x.ING_PRODUCTO_ID == item.ING_PRODUCTO_ID).ING_MEDIDA = medida;
            //    lstDatos.Find(x => x.ING_PRODUCTO_ID == item.ING_PRODUCTO_ID).ING_PROVEEDOR = proveedor;

            //}



            //return _context.ING_PRODUCTO.ToList();
            return categoria;

        }
    }
}
