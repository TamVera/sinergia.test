﻿using datos.inventario;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace api.inventario.Controllers
{
    public class MarcaController : ApiController
    {
        private MoldeSinergiaInventario _context = new MoldeSinergiaInventario();

        [HttpGet]
        public IEnumerable<ING_MARCA> Get()
        {
            //ING_MARCA marca = new ING_MARCA();
            //List<ING_CATEGORIA> categoria = new List<ING_CATEGORIA>();
            List<ING_MARCA> marca = new List<ING_MARCA>();
            //ING_PROVEEDOR proveedor = new ING_PROVEEDOR();

            //SqlParameter paramMarca = null;
            //SqlParameter paramCategoria = null;
            //SqlParameter paramMedida = null;
            SqlParameter paramMARCA = null;

            paramMARCA = new SqlParameter("@P_ID_MARCA", DBNull.Value);
            marca = _context.Database.SqlQuery<ING_MARCA>("dbo.SP_MARCA /*@origen, @periodo*/"
                                                                    , paramMARCA/*, param1*/).ToList();

            return marca;

        }
    }
}
