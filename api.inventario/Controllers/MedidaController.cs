﻿using datos.inventario;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace api.inventario.Controllers
{
    public class MedidaController : ApiController
    {
        private MoldeSinergiaInventario _context = new MoldeSinergiaInventario();

        [HttpGet]
        public IEnumerable<ING_MEDIDA> Get()
        {
            //ING_MARCA marca = new ING_MARCA();
            //List<ING_CATEGORIA> categoria = new List<ING_CATEGORIA>();
            List<ING_MEDIDA> medida = new List<ING_MEDIDA>();
            //ING_PROVEEDOR proveedor = new ING_PROVEEDOR();

            //SqlParameter paramMarca = null;
            //SqlParameter paramCategoria = null;
            SqlParameter paramMedida = null;
            //SqlParameter paramProveedor = null;

            paramMedida = new SqlParameter("@P_ID_MEDIDA", DBNull.Value);
            medida = _context.Database.SqlQuery<ING_MEDIDA>("dbo.SP_MEDIDA /*@origen, @periodo*/"
                                                                    , paramMedida/*, param1*/).ToList();

            return medida;

        }
    }
}
