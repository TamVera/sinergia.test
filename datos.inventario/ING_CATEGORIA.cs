namespace datos.inventario
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ING_CATEGORIA
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ING_CATEGORIA()
        {
            ING_PRODUCTO = new HashSet<ING_PRODUCTO>();
        }

        [Key]
        public int ING_CATEGORIA_ID { get; set; }

        public string ING_DESCRIPCION_CATEGORIA { get; set; }

        [StringLength(1)]
        public string ING_ESTADO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ING_PRODUCTO> ING_PRODUCTO { get; set; }
    }
}
