namespace datos.inventario
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ING_PRODUCTO
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Order = 0)]
        public int ING_PRODUCTO_ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(100)]
        public string ING_PRODUCTO_CODIGO { get; set; }

        public int ING_CATEGORIA_ID { get; set; }

        public int ING_PROVEEDOR_ID { get; set; }

        public int ING_MARCA_ID { get; set; }

        public int ING_MEDIDA_ID { get; set; }

        [Required]
        public string ING_DESCRIPCION { get; set; }

        public int ING_CANTIDAD { get; set; }

        public float ING_PRECIO_UNITARIO { get; set; }

        public virtual ING_CATEGORIA ING_CATEGORIA { get; set; }

        public virtual ING_MARCA ING_MARCA { get; set; }

        public virtual ING_MEDIDA ING_MEDIDA { get; set; }

        public virtual ING_PROVEEDOR ING_PROVEEDOR { get; set; }
    }
}
