namespace datos.inventario
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class MoldeSinergiaInventario : DbContext
    {
        public MoldeSinergiaInventario()
            : base("name=MoldeSinergiaInventario1")
        {
        }

        public virtual DbSet<ING_CATEGORIA> ING_CATEGORIA { get; set; }
        public virtual DbSet<ING_MARCA> ING_MARCA { get; set; }
        public virtual DbSet<ING_MEDIDA> ING_MEDIDA { get; set; }
        public virtual DbSet<ING_PRODUCTO> ING_PRODUCTO { get; set; }
        public virtual DbSet<ING_PROVEEDOR> ING_PROVEEDOR { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ING_CATEGORIA>()
                .Property(e => e.ING_ESTADO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ING_CATEGORIA>()
                .HasMany(e => e.ING_PRODUCTO)
                .WithRequired(e => e.ING_CATEGORIA)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ING_MARCA>()
                .Property(e => e.ING_ESTADO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ING_MARCA>()
                .HasMany(e => e.ING_PRODUCTO)
                .WithRequired(e => e.ING_MARCA)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ING_MEDIDA>()
                .Property(e => e.ING_ESTADO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ING_MEDIDA>()
                .HasMany(e => e.ING_PRODUCTO)
                .WithRequired(e => e.ING_MEDIDA)
                .HasForeignKey(e => e.ING_MEDIDA_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ING_PRODUCTO>()
                .Property(e => e.ING_PRODUCTO_CODIGO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ING_PROVEEDOR>()
                .Property(e => e.ING_RUC)
                .IsFixedLength();

            modelBuilder.Entity<ING_PROVEEDOR>()
                .Property(e => e.ING_ESTADO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ING_PROVEEDOR>()
                .HasMany(e => e.ING_PRODUCTO)
                .WithRequired(e => e.ING_PROVEEDOR)
                .WillCascadeOnDelete(false);
        }
    }
}
