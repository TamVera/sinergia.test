namespace datos.inventario
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ING_PROVEEDOR
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ING_PROVEEDOR()
        {
            ING_PRODUCTO = new HashSet<ING_PRODUCTO>();
        }

        [Key]
        public int ING_PROVEEDOR_ID { get; set; }

        [Required]
        [StringLength(10)]
        public string ING_RUC { get; set; }

        public string ING_DETALLE_PROVEEDOR { get; set; }

        [Required]
        [StringLength(1)]
        public string ING_ESTADO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ING_PRODUCTO> ING_PRODUCTO { get; set; }
    }
}
